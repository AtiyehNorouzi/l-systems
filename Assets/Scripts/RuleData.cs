﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/RuleData", order = 1)]
public class RuleData : ScriptableObject
{
    #region FIELDS
    public string axiom;
    public Rule[] rules;
    #endregion

    #region PUBLIC METHODS
    public string GetRules(Rule[] rules)
    {
        var rulesStr = "";
        foreach (Rule l in rules)
            rulesStr += l.symbol + " -> " + l.rule + "\n";
        return rulesStr;
    }
    #endregion
}

#region STRUCTS
[System.Serializable]
public struct Rule
{
    public char symbol;
    public string rule;
}
#endregion