﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
public class LSystem : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private Slider iterationSlider;

    [SerializeField]
    private Text axiom;

    [SerializeField]
    private Text rulesText;

    [SerializeField]
    private Slider angleSlider;

    [SerializeField]
    private Slider widthSlider;

    [SerializeField]
    private GameObject tree;

    [SerializeField]
    private GameObject leaf;

    [SerializeField]
    private Button generateBtn;

    [SerializeField]
    private Dropdown options;

    [SerializeField]
    private List<RuleData> treeData = new List<RuleData>();

    [SerializeField]
    private Material treeMat;
    #endregion

    #region PRIVATE FIELDS
    private LSystemState state = new LSystemState();
    private Stack<LSystemState> savedState = new Stack<LSystemState>();
    List<GameObject> BranchInstances = new List<GameObject>();
    List<GameObject> leafInstances = new List<GameObject>();
    List<int> leafs = new List<int>();
    List<int> seedSet = new List<int> { 2, 120, 63, 23, 53 };
    List<int> seedList = new List<int>();
    private int iterations;
    private float angle;
    private float width;
    private Vector3 initialPosition;
    int currentLine;
    private string currentPath = "";
    #endregion

    #region MONO BEHAVIOURS
    private void Awake()
    {
        generateBtn.onClick.AddListener(Generate);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
            Generate();
        axiom.text = treeData[options.value].axiom;
        rulesText.text = treeData[options.value].GetRules(treeData[options.value].rules);
    }
    #endregion

    #region PRIVATE METHODS
    private void Init()
    {
        ClearPrevTree();
        savedState.Clear();
        leafs.Clear();
        seedList.Clear();
        iterations = (int)(iterationSlider.value);
        angle = angleSlider.value;
        width = widthSlider.value;
    }

    void ClearPrevTree()
    {
        foreach (GameObject go in BranchInstances)
            Destroy(go);
        foreach (GameObject go in leafInstances)
            Destroy(go);
    }

    private void Generate()
    {
        Init();
        int index = options.value;
        List<Rule> rules = new List<Rule>(treeData[index].rules);
        currentPath = treeData[index].axiom;
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < iterations; i++)
        {
            char[] currentPathChars = currentPath.ToCharArray();
            for (int j = 0; j < currentPathChars.Length; j++)
            {

                string rule = GetRule(currentPathChars[j], rules);
                stringBuilder.Append(rule != null ? rule : currentPathChars[j].ToString());

            }
            currentPath = stringBuilder.ToString();
            stringBuilder.Clear();
        }
        DrawLines(currentPath);
    }

    void DrawLines(string sentence)
    {
        state = new LSystemState()
        {
            x = 0,
            y = 0,
            size = 1,
            angle = 0
        };

        for (int i = 0; i < sentence.Length; i++)
        {
            char c = sentence[i];
            switch (c)
            {
                case 'F':
                    Line();
                    break;
                case 'X':
                    Translate();
                    break;
                case '+':
                    state.angle += angle;
                    break;
                case '-':
                    state.angle -= angle;
                    break;
                case '[':
                    savedState.Push(state.Clone());
                    break;
                case ']':
                    state = savedState.Pop();
                    break;
                default:
                    break;
            }
        }
    }
    void Line()
    {
        var lineGo = new GameObject($"Line_{currentLine}");
        lineGo.transform.position = Vector3.zero;
        lineGo.transform.parent = tree.transform;
        BranchInstances.Add(lineGo);
        LineRenderer newLine = SetupLine(lineGo);
        newLine.SetPosition(0, new Vector3(state.x + tree.transform.position.x, state.y + tree.transform.position.y, tree.transform.position.z));
        CheckAngles(true);
        newLine.SetPosition(1, new Vector3(state.x + tree.transform.position.x, state.y + tree.transform.position.y, tree.transform.position.z));
        currentLine++;
    }

    void Translate() => CheckAngles(false);
    private void CheckAngles(bool leafCheck)
    {
        if (state.angle != 0)
        {
            state.x += float.Parse((Mathf.Sin(state.angle / 100)).ToString());
            state.y += float.Parse((Mathf.Cos(state.angle / 100)).ToString());
            if (leafCheck)
            {
                var leafCount = Random.Range(1, 4);
                for (int i = 0; i < leafCount; i++)
                {
                    Random.seed = NewSeed();
                    var leafGO = Instantiate(leaf);
                    leafInstances.Add(leafGO);
                    leafGO.transform.position = new Vector3(state.x + tree.transform.position.x, state.y + tree.transform.position.y, 0);
                    leafGO.transform.rotation = Quaternion.Euler(0, 0, Random.Range(-120, 90));
                }
                seedList.Clear();
            }
        }
        else
        {
            state.y = state.y + state.size;
        }
    }


    int NewSeed()
    {
        int newseed = seedSet[Random.Range(0, seedSet.Count)];
        while (seedList.Contains(newseed))
            newseed = seedSet[Random.Range(0, seedSet.Count)];
        seedList.Add(newseed);
        return newseed;
    }

    private LineRenderer SetupLine(GameObject lineGo)
    {
        LineRenderer newLine = lineGo.AddComponent<LineRenderer>();
        newLine.useWorldSpace = true;
        newLine.positionCount = 2;
        newLine.material = treeMat;
        newLine.startWidth = width;
        newLine.endWidth = width;
        newLine.numCapVertices = 5;
        return newLine;
    }

    string GetRule(char character, List<Rule> rules)
    {
        foreach (Rule r in rules)
        {
            if (r.symbol.Equals(character))
                return r.rule;
        }
        return null;
    }
    #endregion
}
