﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChangingText : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private Slider slider;
    #endregion

    #region FIELDS
    Text handleText;
    #endregion

    #region MONO BEHAVIOURS
    private void Start()
    {
        handleText = GetComponent<Text>();
    }

    void Update()
    {
        handleText.text = slider.value.ToString();
    }
    #endregion
}
