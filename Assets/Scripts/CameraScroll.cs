﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour
{
    #region PUBLIC METHODS
    public float targetZoom;

    public float zoomFactor = 3f;
    #endregion

    #region MONO BEHAVIOURS
    private void Start()
    {
        targetZoom = Camera.main.orthographicSize;
    }

    void Update()
    {
        float scrollData;
        scrollData = Input.GetAxis("Mouse ScrollWheel");

        targetZoom -= scrollData * zoomFactor;

        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, targetZoom, Time.deltaTime);

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Camera.main.transform.position += Time.deltaTime * Vector3.up;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            Camera.main.transform.position += Time.deltaTime * Vector3.down;
        }
    }
    #endregion
}
